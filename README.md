# HyAO

HyAO consists of a collection of AO transformation tools, and is set up in
order to allow for easy mixing and matching of integrals, matrices and
tensors from different quantum chemistry codes. HyAO has been developed
as part of the Hylleraas Software Platform, but is constructed for use
also as a stand-alone package.

Documentation of the Hylleraas Software Platform can be found
[here](https://hylleraas.readthedocs.io/en/latest/).


# Installation instructions

The HyAO package is not yet released, and is to be considered work in progress.
As such, we do not yet support PyPI installation for example.

At current, the code can be installed directly from the gitlab source
```
pip install git+https://gitlab.com/hylleraasplatform/hyao.git
```

# Usage

In order to convert a NumPy tensor `tensor_original` from say LSDalton AO ordering
to VeloxChem ordering, use the `ao_transformation` routine according to, for
example

```
from hyao import ao_transformation

xyz = """3
Water comment line
H      0.757160     0.586260     0.000000
O      0.000000     0.000000     0.000000
H     -0.757160     0.586260     0.000000
"""
basis = "pcseg-1"

tensor_tranformed = ao_transformation(tensor_original, xyz, basis,
    "veloxchem", "lsdalton")
```

with `xyz` a string defining the xyz-format of the molecule, and
`basis` the string defining the basis-set name of the system.
Here the neccessary basis set information is taken from the
`basis_set_exchange` Python package. As such this way of using the
HyAO is only supported for basis sets that are supported
through `basis_set_exchange`. Furthermore, it is assumed each mode
has the correct dimensions (for the example system considered here
$n_{AO}=24$).

In the above, each mode of the tensor is transformed, but it is
also possible to transform a subset of the modes. This can be
done by adding an optional `indeces` tuple specifying which modes
to transform. For instance the transformation can be limited to the
first and third mode of the tensor through
```
tensor_tranformed = ao_transformation(tensor_original, xyz, basis,
    "veloxchem", "lsdalton", indeces = (0,2))
```
# Advanced

It is also possible to define the system through `atoms` and
`shell_lists`, rather than `xyz` and `basis` for more control. Here
the `shell_lists` defines a list of atom types, with each type defined
by a tuple of shells with increasing angular momenta. We can replicate
the example above using instead

```
from hyao import ao_transformation_shell_list

atoms = [0, 1, 0]
shell_lists = [(2,1), (3,2,1)]

tensor_tranformed = ao_transformation_shell_list(tensor_original,
    atoms, shell_lists, "veloxchem", "lsdalton")
```


Here the tuples `(2,1)` and `(3,2,1)` specifies that there are two s-shells
and one p-shell for atom type `0` and three s-shells, two p-shells and one
d-shell for atom type `1`, and the atom list `[0, 1, 0]` specify that the
first and thrird atom is of atom type `0`, and that the second is of
type `1`.

It is also possible to calculated the transformation matrix directly,
through either `t_mat`, according to
```
from hyao import t_mat

t_mat = t_mat(xyz, basis, "veloxchem", "lsdalton")
```
or `t_mat_shell_list`, through
```
from hyao import t_mat_shell_list

t_mat = t_mat_shell_list(atoms, shell_lists, "veloxchem", "lsdalton")
```
The matrix can then be freely reused to perform tranformations through
subsequent calls to `t_transform`
```
from hyao import t_transform

tensor_tranformed = t_transform(tensor_original, t_mat, indeces = (0,2))
```
