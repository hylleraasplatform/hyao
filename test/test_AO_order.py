import os

import numpy as np
import pytest

from hyao import AOordering

shell_list = [(3, 2, 1), (3, 2, 1, 1)]

mol1 = [0, 1, 0]
mol2 = [0, 1, 0, 0, 1, 0]


@pytest.fixture(scope='session', params=[mol1, mol2])
def get_mol(request):
    """Fixture returning molecule specification."""
    yield request.param


def test_allowed_orders():
    """Test the list of allowed predefined AO orders"""
    assert AOordering.allowed_orders() == [
        'default', 'orca', 'tubomole', 'pyscf', 'dalton', 'lsdalton',
        'gaussian', 'veloxchem', 'gbasis', 'molpro'
    ]


def test_ao_ordering(get_mol):
    """Test AOordering creation"""
    ao = AOordering(get_mol, shell_list)
    assert ao.atoms == mol1 or ao.atoms == mol2
    assert ao.num_atoms == 3 or ao.num_atoms == 6
    assert ao.num_AOtypes == 2
    assert ao.num_ao == 49 or ao.num_ao == 98
    assert ao.ordering == 0
    assert ao.atomtypes[0].num_ao == 14 and \
           ao.atomtypes[1].num_ao == 21


def test_ao_transformation_matrix(get_mol):
    """Test the AO transformation matrix between two AO representations"""
    for o1 in AOordering.allowed_orders():
        ao1 = AOordering(get_mol, shell_list, order=o1)
        for o2 in AOordering.allowed_orders():
            ao2 = AOordering(get_mol, shell_list, order=o2)
            ref_file = os.path.join(pytest.FIXTURE_DIR,
                                    f'{o1}_{o2}_{len(get_mol)}_t_ao.npy')
            t_mat = AOordering.ao_transformation_matrix(ao1, ao2)
            reference = np.load(ref_file)
            assert np.array_equal(t_mat, reference)
            # np.save(ref_file, t_mat)
            # assert 1 == 1
