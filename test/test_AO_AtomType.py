import os
import re

import numpy as np
import pytest

from hyao import AOAtomType

shell_list_1 = (5, 3, 2, 1)
shell_list_2 = (5, 4, 3, 2, 1, 1)


@pytest.fixture(scope='session',
                params=[('label1', shell_list_1), ('label2', shell_list_2)])
def get_shell_list(request):
    """Fixture returning shell_list string with a label."""
    yield request.param[1]


@pytest.fixture(scope='session',
                params=[('label1', shell_list_1), ('label2', shell_list_2)])
def get_label(request):
    """Fixture returning label string of the fixture."""

    yield request.param[0]


def test_allowed_orders():
    """Test the list of allowed predefined AO orders"""

    assert AOAtomType.allowed_orders() == [
        'default', 'orca', 'tubomole', 'pyscf', 'dalton', 'lsdalton',
        'gaussian', 'veloxchem', 'gbasis', 'molpro'
    ]


def test_ao_atomtype(get_shell_list):
    """Test AOAtomType creation"""

    atom = AOAtomType(get_shell_list)
    assert atom.max_angmom == 3 or atom.max_angmom == 5
    assert atom.num_ao == 31 or atom.num_ao == 66
    assert atom.ordering == 0
    assert atom.ao_order == [
        [0], [-1, 0, 1], [-2, -1, 0, 1, 2], [-3, -2, -1, 0, 1, 2, 3]
    ] or atom.ao_order == [[0], [-1, 0, 1], [-2, -1, 0, 1, 2],
                           [-3, -2, -1, 0, 1, 2, 3],
                           [-4, -3, -2, -1, 0, 1, 2, 3, 4],
                           [-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5]]
    assert atom.ao_phase == [[1.0], [1.0, 1.0, 1.0], [
        1.0, 1.0, 1.0, 1.0, 1.0
    ], [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0]] or atom.ao_phase == [
        [1.0], [1.0, 1.0, 1.0], [1.0, 1.0, 1.0, 1.0, 1.0],
        [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
        [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
        [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0]
    ]


def test_ao_transformation_matrix(get_shell_list):
    """Test AO transformation matrix between two AO bases"""

    for o1 in AOAtomType.allowed_orders():
        atom1 = AOAtomType(get_shell_list, order=o1)
        for o2 in AOAtomType.allowed_orders():
            atom2 = AOAtomType(get_shell_list, order=o2)
            ref_file = os.path.join(
                pytest.FIXTURE_DIR,
                f'{o1}_{o2}_{len(get_shell_list)}_t_mat.npy')
            t_mat = AOAtomType.atom_ao_transformation_matrix(atom1, atom2)
            reference = np.load(ref_file)
            assert np.array_equal(t_mat, reference)
            # np.save(ref_file, t_mat)
            # assert 1 == 1


def test_invalid_order_exception():
    """Test AOAtomType creation with an invalid order"""
    error_message = 'Non-existing AO order xyzxyz. Allowed preset orders are'
    error_message += f' {AOAtomType.allowed_orders()}'
    with pytest.raises(ValueError, match=re.escape(error_message)):
        AOAtomType(shell_list_1, order='xyzxyz')
