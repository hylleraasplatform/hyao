import os
from dataclasses import dataclass

import numpy as np
import pytest

from hyao import allowed_orders, ao_transformation


@dataclass
class Molecule:
    """Interface to hylleraas Molecule class."""

    atoms: list

    def count(self, search_str: str) -> int:
        count = 2
        if len(self.atoms) == 3:
            count = 5
        return count


mol1 = """Li 0.0 0.0 0.0
H  0.0 0.0 1.5
"""

mol2 = """3

H     0.757163     0.000000    -0.469428
O     0.000000     0.000000     0.117073
H    -0.757163     0.000000    -0.469428
"""

mol3 = Molecule(atoms=['Li', 'H'])

mol4 = Molecule(atoms=['H', 'O', 'H'])

bas1 = 'pcseg-0'
bas2 = 'pcseg-1'


@pytest.fixture(scope='session', params=[mol1, mol2, mol3, mol4])
def get_mol(request):
    """Fixture returning molecule specification."""
    yield request.param


@pytest.fixture(scope='session', params=[bas1, bas2])
def get_bas(request):
    """Fixture returning basis set specification."""
    yield request.param


def test_allowed_orders():
    """Test the list of allowed predefined AO orders"""
    assert allowed_orders() == [
        'default', 'orca', 'tubomole', 'pyscf', 'dalton', 'lsdalton',
        'gaussian', 'veloxchem', 'gbasis', 'molpro'
    ]


def test_ao_transformation(get_mol, get_bas):
    """Test AO transformation"""

    o2 = 'Default'
    nmol = get_mol.count('\n')
    o2_file = os.path.join(pytest.FIXTURE_DIR, f'{nmol}_{get_bas}_ref.npy')
    t2_ref = np.load(o2_file)
    for o1 in allowed_orders():
        # Full transformations
        o1_file = os.path.join(pytest.FIXTURE_DIR,
                               f'{o1}_{nmol}_{get_bas}.npy')

        # There ...
        t1 = ao_transformation(t2_ref, get_mol, get_bas, o1, o2)
        t1_ref = np.load(o1_file)
        assert np.array_equal(t1, t1_ref)
        # np.save(o1_file,t1)
        # assert 1 == 1

        # ... and back again
        t2 = ao_transformation(t1, get_mol, get_bas, o2, o1)
        assert np.array_equal(t2, t2_ref)

        # Partial transformations
        o1_file = os.path.join(pytest.FIXTURE_DIR,
                               f'ind_{o1}_{nmol}_{get_bas}.npy')

        t1 = ao_transformation(t2_ref,
                               get_mol,
                               get_bas,
                               o1,
                               o2,
                               indeces=(0, 2))
        t1_ref = np.load(o1_file)
        assert np.array_equal(t1, t1_ref)
        # np.save(o1_file,t1)
        # assert 1 == 1

        t2 = ao_transformation(t1, get_mol, get_bas, o2, o1, indeces=(0, 2))
        assert np.array_equal(t2, t2_ref)
