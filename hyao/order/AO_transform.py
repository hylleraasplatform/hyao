import contextlib

import basis_set_exchange as bse
import numpy as np
import qcelemental

from .AO_order import AOordering


def ao_transformation(tensor,
                      molecule,
                      basis,
                      order_new,
                      order_old,
                      indeces=None):
    """AO transform one or more modes a tensor.

    See allowed_orders() for allowed orders.

    Parameters
    ----------
    tensor : np.array
        The tensor to be transformed
    molecule : Union[Molecule, str]
        Molecule in xyz format or Molecule object
    basis : str
        Basis set name
    order_new: str
        Speficiy the AO order to transform to
    order_old: str
        Speficiy the AO order to transform from
    indeces: tuple
        Speficiy which AO modes to tansform

    Returns
    -------
    np.array
        The transformed tensor

    """
    augmentation_level = __get_augmentation_level(basis)
    atoms, shell_lists = __get_atoms_and_shell_lists(molecule, basis)
    return ao_transformation_shell_list(tensor, atoms, shell_lists, order_new,
                                        order_old, indeces, augmentation_level)


def ao_transformation_shell_list(tensor,
                                 atoms,
                                 shell_lists,
                                 order_new,
                                 order_old,
                                 indeces=None,
                                 augmentation_level=0):
    """AO transform one or more modes a tensor.

    See allowed_orders() for allowed orders.

    Parameters
    ----------
    tensor : np.array
        The tensor to be transformed
    atoms: tuple
        For each atoms specify corresponding shell_lists
    shell_lists: list
        For each atom type specify the tuple (s,p,d,...)
    order_new: str
        Speficiy the AO order to transform to
    order_old: str
        Speficiy the AO order to transform from
    indeces: tuple
        Speficiy which AO modes to tansform
    augmentation_level: int
        Speficiy augmentation level (1 for aug-, 2 for d-aug etc.)

    Returns
    -------
    np.array
        The transformed tensor

    """
    t = t_mat_shell_list(atoms, shell_lists, order_new, order_old,
                         augmentation_level)
    return t_transform(tensor, t, indeces)


def t_mat(molecule, basis, order_new, order_old):
    """Calculate a tranformation matrix.

    See AOordering for allowed orders.

    Parameters
    ----------
    molecule : Union[Molecule, str]
        Molecule in xyz format or Molecule object
    basis : str
        Basis set name
    order_new: str
        Speficiy the AO order to transform to
    order_old: str
        Speficiy the AO order to transform from

    Returns
    -------
    np.array
        The transformation matrix

    """
    augmentation_level = __get_augmentation_level(basis)
    atoms, shell_lists = __get_atoms_and_shell_lists(molecule, basis)
    return t_mat_shell_list(atoms, shell_lists, order_new, order_old,
                            augmentation_level)


def t_mat_shell_list(atoms,
                     shell_lists,
                     order_new,
                     order_old,
                     augmentation_level=0):
    """Calculate a tranformation matrix.

    See AOordering for allowed orders.

    Parameters
    ----------
    atoms: tuple
        For each atoms specify corresponding shell_lists
    shell_lists: list
        For each atom type specify the tuple (s,p,d,...)
    order_new: str
        Speficiy the AO order to transform to
    order_old: str
        Speficiy the AO order to transform from
    augmentation_level: int
        Speficiy augmentation level (1 for aug-, 2 for d-aug etc.)

    Returns
    -------
    np.array
        The transformation matrix

    """
    ao1 = AOordering(atoms,
                     shell_lists,
                     order_new,
                     augmentation_level=augmentation_level)
    ao2 = AOordering(atoms,
                     shell_lists,
                     order_old,
                     augmentation_level=augmentation_level)
    return AOordering.ao_transformation_matrix(ao1, ao2)


def t_transform(tensor, t_mat, indeces=None):
    """Transform one or more modes a tensor.

    Parameters
    ----------
    tensor : np.array
        The tensor to be transformed
    t_mat: np.array
        Specifies transformation matrix
    indeces : tuple
        Specifies which modes to transform

    Returns
    -------
    np.array
        The transformed tensor

    """
    n = t_mat.shape[1]

    if indeces is None:
        # Full transformation by default
        t_indeces = tuple(range(tensor.ndim))
    else:
        # User speified transformation
        t_indeces = indeces

    # Make the transformation (one index at a time)
    t1 = tensor
    for i in t_indeces:
        if n != t1.shape[i]:
            raise Exception('Mismatching dimension!')

        t1 = np.moveaxis(t1, i, 0)
        t1 = np.tensordot(t_mat, t1, axes=([1], [0]))
        t1 = np.moveaxis(t1, 0, i)

    return t1


def allowed_orders():
    """Return list of allowed orders.

    Returns
    -------
    list
        Implemented AO orderings

    """
    return AOordering.allowed_orders()


def __get_atoms_and_shell_lists(molecule, basis):
    """Set up the atoms and shell_lists.

    Parameters
    ----------
    molecule : Union[Molecule, str]
        Molecule in xyz format or of type Molecule
    basis : str
        Basis set name

    Returns
    -------
    tuple
        For each atoms specify corresponding shell_lists
    list
        For each atom type specify the tuple (s,p,d,...)

    """
    if isinstance(molecule, str):
        lines = molecule.strip().split('\n')

        elements = []
        start_line = 0
        with contextlib.suppress(ValueError):
            int(lines[0])
            start_line = 2
        for line in lines[start_line:]:
            element = line.split()[0]
            elements.append(element)
    else:
        elements = molecule.atoms

    unique = sorted(set(elements))
    element_map = {element: idx for idx, element in enumerate(unique)}
    atom_map = {
        element: qcelemental.periodictable.to_Z(element)
        for element in unique
    }
    atoms = [element_map[element] for element in elements]

    basis_dict = bse.get_basis(basis, elements=unique)
    shell_lists = []
    for element in unique:
        electron_shell = basis_dict['elements'][f'{atom_map[element]}']
        max_angular = max(
            int(shell['angular_momentum'][0])
            for shell in electron_shell['electron_shells'])
        shell_list = [0] * (max_angular+1)
        for shell in electron_shell['electron_shells']:
            contracted = sum(1 for _ in shell['coefficients'])
            shell_list[shell['angular_momentum'][0]] += contracted
        shell_lists.append(tuple(shell_list))

    return atoms, shell_lists


def __get_augmentation_level(basis):
    """Return the augmentation level from the provided basis.

    Parameters
    ----------
    basis : str
        Basis set name

    Returns
    -------
    augmentation_level: int
        The augmentation level (1 for aug-, 2 for d-aug etc.)

    """
    augmentation_level = 0

    # Check the prefix of the basis string
    if basis.startswith('aug-'):
        augmentation_level = 1
    elif basis.startswith('d-aug-'):
        augmentation_level = 2

    return augmentation_level
