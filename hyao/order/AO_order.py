import numpy as np

from .AO_AtomType import AOAtomType

ATOM_L_N_ML = 0
L_ML_ATOM_N = 1


class AOordering:
    """Keep track of AO ordering for molecule with given shell structure."""

    def __init__(
        self,
        atoms,
        shell_lists,
        order='Default',
        augmentation_level=0,
    ):
        """Keep track of the AO component ordering for a molecule.

        Default ordering is atomwise, with each atomic block ordered according
        to the ordering of each atomic type (as specified by the shell_lists,
        and the order).

        Predefined orders can be choosen from the list:

             Default, Orca, Tubomole, PySCF, VeloxChem, Dalton, LSDalton

        Example: water in 6-31G basis with Orca order.

        In this example the atomic order of water is {O, H, H}, for example as
        specified by the xyz input

            O  0.000000  0.000000  0.000000
            H  0.758602  0.000000  0.504284
            H  0.758602  0.000000  -0.504284

        For the 6-31G basis we have the following number of subshells for O and
        H:

            O 3s,2p
            H 2s

        Input to specify this system is given by

            atoms = [0, 1, 1],
            shell_lists = [(3,2), (2)],
            order = "Orca"

        Parameters
        ----------
        atoms : tuple
            Atom list, with each entry specifies the AO_list tuple to use (for
            given atom).  The number of shells, defined by the tuple (s, p, d,
            ...)
        shell_lists : list
            List of distinct atomic shell tuples, each defined by (s, p, d,
            ...)
        order : str
            Specifies predefined AO orderings (see options above)
        augmentation_level: int
            Speficiy augmentation level (1 for aug-, 2 for d-aug etc.)


        """
        if order.lower() not in self.allowed_orders():
            raise Exception(f'Provided order {order} not in allowed list: '
                            f'{self.allowed_orders()})')

        if (min(atoms) < 0) or (max(atoms) >= len(shell_lists)):
            raise Exception(
                f'Provided atoms should only contain values in the range from '
                f'0 to len(shell_lists): min={min(atoms)}, max={max(atoms)}, '
                f'len={len(shell_lists)} ')

        self.atoms = atoms
        self.atomtypes = [
            AOAtomType(shell_list,
                       order=order,
                       augmentation_level=augmentation_level)
            for shell_list in shell_lists
        ]
        self.num_atoms = len(atoms)
        self.num_AOtypes = len(shell_lists)

        self.num_ao = self.__num_ao()
        if order.lower() == 'veloxchem':
            self.ordering = L_ML_ATOM_N
        else:
            self.ordering = ATOM_L_N_ML

        self.augmentation_level = augmentation_level

    def __repr__(self):
        """Print routine."""
        if self.ordering == ATOM_L_N_ML:
            ordering = 'atom, l, ml, n'
        elif self.ordering == L_ML_ATOM_N:
            ordering = 'l, ml, atom, n'
        return (f'AOordering(num_atoms={self.num_atoms}, '
                f'num_ao={self.num_ao}, '
                f'num_AOtypes={self.num_AOtypes}, '
                f'ordering=({ordering}), '
                f'augmentation_level={self.augmentation_level}, '
                f'atoms={self.atoms})')

    def __num_ao(self):
        """Return number of AOs."""
        num_ao = 0
        for atom in range(0, self.num_atoms):
            at = self.atoms[atom]
            num_ao = num_ao + self.atomtypes[at].num_ao
        return num_ao

    def __make_tuple_ao(self):
        """Set up a list a list of unique tuples for each AO index.

        Parameters
        ----------
        atom : int
            Atomic index. Needed for a global mapping only

        """
        ao = 0
        tuple_ao = [0] * self.num_ao
        if self.ordering == L_ML_ATOM_N:
            # Find the maximum angular momenta
            max_angmom = -1
            at_max = -1
            for at in range(0, self.num_AOtypes):
                if self.atomtypes[at].max_angmom > max_angmom:
                    at_max = at
                    max_angmom = self.atomtypes[at].max_angmom

            # Create dictionary for list of atoms for each angular momenta,
            # and dictionary with list of the ao_order for each angular
            # momentum
            atoms = {}
            ao_order = {}
            for ang in range(0, max_angmom + 1):
                atoms[ang] = []
                ao_order[ang] = self.atomtypes[at_max].ao_order[ang]

            # Consistency check for each ao_order
            for at in range(0, self.num_AOtypes):
                for ang in range(0, self.atomtypes[at].max_angmom + 1):
                    if self.atomtypes[at].ao_order[ang] != ao_order[ang]:
                        raise Exception(
                            'Entered not-implemented option (ao_order not '
                            'equal) in make_tuple_ao for case L_ML_ATOM_N.')

            # Count the number of atoms with given angular momenta, and make
            # a corresponding mapping of their atomic indeces
            natoms = [0] * (max_angmom+1)
            for atom in range(0, len(self.atoms)):
                atomtype = self.atomtypes[self.atoms[atom]]
                for ang in range(0, atomtype.max_angmom + 1):
                    natoms[ang] = natoms[ang] + 1
                    atoms[ang].append(atom)

            # Make a loop to contruct the tuple list
            for ang in range(0, max_angmom + 1):
                for ml in ao_order[ang]:
                    for atom in range(0, natoms[ang]):
                        at = self.atoms[atoms[ang][atom]]
                        atomtype = self.atomtypes[at]
                        for n in range(0, atomtype.ao_list[ang]):
                            tuple_ao[ao] = (atoms[ang][atom], ang, n, ml)
                            ao = ao + 1
        elif self.ordering == ATOM_L_N_ML:
            for atom in range(0, len(self.atoms)):
                atomtype = self.atomtypes[self.atoms[atom]]
                for ang in range(0, atomtype.max_angmom + 1):
                    for n in range(0, atomtype.ao_list[ang]):
                        for ml in atomtype.ao_order[ang]:
                            tuple_ao[ao] = (atom, ang, n, ml)
                            ao = ao + 1
        return tuple_ao

    def __make_phase_ao(self):
        """Return the phase of each AO."""
        if self.ordering == L_ML_ATOM_N:
            # Currently only veloxchem, with only positive phases
            phase_ao = [1.0] * self.num_ao
        elif self.ordering == ATOM_L_N_ML:
            phase_ao = [1.0] * self.num_ao
            ao = 0
            for atom in range(0, len(self.atoms)):
                atomtype = self.atomtypes[self.atoms[atom]]
                for ang in range(0, atomtype.max_angmom + 1):
                    for n in range(0, atomtype.ao_list[ang]):
                        for phase in atomtype.ao_phase[ang]:
                            phase_ao[ao] = phase
                            ao = ao + 1
        return phase_ao

    @staticmethod
    def __ao_mapping(ao1, ao2):
        """Make index and phase mapping between two AO ordering types.

        Parameters
        ----------
        ao1 : AOordering
            The first AO ordering
        ao2 : AOordering
            The second AO ordering

        """
        if ao1.atoms != ao2.atoms and ao1.shell_lists != ao2.shell_lists:
            raise ValueError('Mismatch between the two AOordering types to be'
                             ' mapped')
        mapping = [0] * ao1.num_ao
        phase = [1.0] * ao1.num_ao
        tuple1 = ao1.__make_tuple_ao()
        tuple2 = ao2.__make_tuple_ao()
        phase1 = ao1.__make_phase_ao()
        phase2 = ao2.__make_phase_ao()

        for i in range(0, ao1.num_ao):
            j = tuple2.index(tuple1[i])
            mapping[i] = j
            phase[i] = phase1[i] * phase2[j]
        return mapping, phase

    @staticmethod
    def allowed_orders():
        """Return the list of predefined AO orders.

        Returns
        -------
        List of predefined AO orders

        """
        return AOAtomType.allowed_orders()

    @staticmethod
    def ao_transformation_matrix(ao1, ao2):
        """Make a transformation matrix to map AOs between two AO orderings.

        Resulting transformation matrix T can transform from ao2 to ao1 by
        acting to the right

             v1 = T * v2

        Parameters
        ----------
        ao1 : AOordering
            The first AO ordering
        ao2 : AOordering
            The second AO ordering

        """
        n = ao1.num_ao
        t_mat = np.zeros((n, n))
        # Atomwise subblocks
        if (ao1.ordering == ATOM_L_N_ML) and (ao2.ordering == ATOM_L_N_ML):
            if ao1.num_atoms != ao2.num_atoms:
                raise Exception('Mismatching number of atoms!')
            if ao1.atoms != ao2.atoms:
                raise Exception('Atom reordering currently not implemented.')
            if ao1.num_ao != ao2.num_ao:
                raise Exception('Mismatching AO dimensions!')
            if len(ao1.atomtypes) != len(ao2.atomtypes):
                raise Exception('Mismatching number of AOtypes!')
            atomic_blocks = []
            for at in range(0, len(ao1.atomtypes)):
                atomic_blocks.append(
                    AOAtomType.atom_ao_transformation_matrix(
                        ao1.atomtypes[at], ao2.atomtypes[at]))
            iao = 0
            for atom in range(0, ao1.num_atoms):
                at = ao1.atoms[atom]
                nao = ao1.atomtypes[at].num_ao
                t_mat[iao:iao + nao, iao:iao + nao] = atomic_blocks[at]
                iao = iao + nao
        elif (ao1.ordering == L_ML_ATOM_N) or (ao2.ordering == L_ML_ATOM_N):
            mapping, phase = AOordering.__ao_mapping(ao1, ao2)
            for i in range(0, n):
                t_mat[i][mapping[i]] = phase[i]
        else:
            raise Exception('Unknown ordering in ao_transformation_matrix')

        return t_mat
