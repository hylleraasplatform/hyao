import numpy as np

ALLOWED_ORDERS = [
    'default', 'orca', 'tubomole', 'pyscf', 'dalton', 'lsdalton', 'gaussian',
    'veloxchem', 'gbasis', 'molpro'
]

L_N_ML = 0
L_ML_N = 1


class AOAtomType:
    """Keeps track of the AO component ordering for an single atom type.

    E.g. with a given combination of nuclear charge and basis.
    """

    def __init__(self, ao_list, order='Default', augmentation_level=0):
        """Keep track of the AO component ordering for an atom type.

        E.g. with a given combination of nuclear charge and basis.

        Default ordering is s-orbitlas first, then all p-orbitals, etc. The
        different angular components are consecutive within each subshell.
        Default angular-component ordering is

            ml = {-l, -l+1, ..., l-1, l}.

        To give an example consider an atom defined by the ao_list (3,2,1).
        With default settings the AO ordered will be according to:

             {1s; 2s; 3s; 1py; 1pz; 1px; 2py; 2pz; 2px;
              1dxy; 1dyz; 1dz2−r2; 1dxz; 1dx2−y2}

        where the p-orbitals are ordered with ml values -1, 0 and 1, and the
        d-orbitals are ordered with ml values -2, -1, 0, 1, 2.

        Predefined orderings can be choosen from the list:

             Default, Orca, Tubomole, MolPRO, PySCF, VeloxChem, Dalton,
             LSDalton
        Parameters
        ----------
        ao_list : tuple
            The number of shells, defined by the tuple (s, p, d, ...)
        order : str
            Specifies predefined AO orderings
        augmentation_level: int
            Speficiy augmentation level (1 for aug-, 2 for d-aug etc.)
        """
        self.max_angmom = len(ao_list) - 1
        self.ao_list = ao_list
        self.num_ao = self.__num_ao()
        self.ordering = L_N_ML
        self.ao_order = self.__default_ao_order(self.max_angmom)
        self.ao_phase = self.__default_ao_phase(self.max_angmom)
        self.augmentation = 0
        if order.lower() == 'default':
            # Do nothing
            pass
        elif order.lower() == 'gbasis':
            # Do nothing
            if augmentation_level >= 1:
                print('HyAO augmented ordering for gbasis unknown')
        elif order.lower() == 'orca':
            self.set_increasing_ml_ordering()
            if augmentation_level >= 1:
                print('HyAO augmented ordering for Orca unknown')
        elif order.lower() == 'tubomole':
            self.set_xyz_increasing_ml_ordering()
            if augmentation_level >= 1:
                print('HyAO augmented ordering for Turbomole unknown')
        elif order.lower() == 'gaussian':
            self.set_xyz_increasing_ml_ordering()
            if augmentation_level >= 1:
                print('HyAO augmented ordering for Gaussian  unknown')
        elif order.lower() == 'pyscf':
            self.set_xyz_negative_ml_ordering()
            self.augmentation = augmentation_level
        elif (order.lower() == 'dalton' or order.lower() == 'lsdalton'):
            self.set_xyz_negative_ml_ordering()
        elif order.lower() == 'molpro':
            self.set_molpro_ordering()
            if augmentation_level >= 1:
                print('HyAO augmented ordering for Molpro  unknown')
        elif order.lower() == 'veloxchem':
            self.set_veloxchem_ordering()
        else:
            raise ValueError(
                f'Non-existing AO order {order}. Allowed preset orders are'
                f' {ALLOWED_ORDERS}')

    def set_increasing_ml_ordering(self):
        """Increasing AO ordering."""
        for ang_mom in range(1, self.max_angmom + 1):
            self.set_order(ang_mom, self.__increasing_ml(ang_mom))

    def set_xyz_increasing_ml_ordering(self):
        """XYZ p, increasing AO ordering d and on."""
        self.set_xyz_order()
        for ang_mom in range(2, self.max_angmom + 1):
            self.set_order(ang_mom, self.__increasing_ml(ang_mom))

    def set_xyz_negative_ml_ordering(self):
        """XYZ p, negative AO ordering d and on."""
        self.set_xyz_order()

    def set_molpro_ordering(self):
        """Molpro AO ordering."""
        self.set_xyz_order()
        for ang_mom in range(2, self.max_angmom + 1):
            self.set_order(ang_mom, self.__alternating_ml(ang_mom))
            self.set_phase(ang_mom, self.__molpro_phase(ang_mom))

    def set_veloxchem_ordering(self):
        """AO ordering for VeloxChem.

        VeloxChem ordering is l, ml, atom, n, with the angular ordering
        according to our default (__negative_ml) ordering, see notes on
        VeloxChem ordering here:

            https://kthpanor.github.io/echem/docs/elec_struct/orbitals.html
        """
        self.ordering = L_ML_N

    def __repr__(self):
        """Print routine."""
        return (f'AOAtomType(num_ao={self.num_ao}, '
                f'max_angmom={self.max_angmom }, '
                f'ao_list={self.ao_list}, '
                f'ao_order={self.ao_order}, '
                f'ao_phase={self.ao_phase}'
                f'augmenation={self.augmentation})')

    def __negative_ml(self, ang_mom):
        """Condon-Shortley convention for the real-solid harmonic ordering.

        The Condon-Shortley convention, is


        ml = {-l, -l+1, ..., l-1, l}

        See:

            https://en.wikipedia.org/wiki/Table_of_spherical_harmonics
        Parameters
        ----------
        ang_mom : int
            The angular momentum quantun number

        """
        return list(range(-ang_mom, ang_mom + 1))

    def __increasing_ml(self, ang_mom):
        """Angular component ordering of the real spherical harmonic.

            ml = {0,+1,-1,+2,-2,...,+l,-l}.

        Parameters
        ----------
        ang_mom : int
            The angular momentum quantun number

        """
        order = [0]
        for i in range(1, ang_mom + 1):
            order.extend((i, -i))
        return order

    def __alternating_ml(self, ang_mom):
        """Angular component ordering of the real spherical harmonic.

            ml = {0,+1,-1,-2,+2,+3,-3,...}.

        Parameters
        ----------
        ang_mom : int
            The angular momentum quantun number

        """
        order = [0]
        phase = -1 if ang_mom % 2 == 0 else 1
        for i in range(1, ang_mom + 1):
            order.extend((phase * i, -phase * i))
        return order

    def __molpro_phase(self, ang_mom):
        """Set the Molpro negative phase factor for f3-, g2+ and g3-.

        Parameters
        ----------
        ang_mom: int
            The angular momentum quantun number

        """
        phase = self.ao_phase[ang_mom]
        if ang_mom == 3:
            phase[6] = -1.0
        elif ang_mom == 4:
            phase[4] = -1.0
            phase[6] = -1.0
        return phase

    def __default_ao_order(self, max_angmom):
        """Set default order for each angular momentum quantum number l.

        Parameters
        ----------
        max_angmom : int
            The maximum angular momentum quantun number

        """
        ao_order = []
        for ang_mom in range(0, max_angmom + 1):
            ao_order.append(self.__negative_ml(ang_mom))
        return ao_order

    def __default_ao_phase(self, max_angmom):
        """Set default phase for each angular momentum quantum number l.

        Parameters
        ----------
        max_angmom : int
            The maximum angular momentum quantun number

        """
        ao_phase = []
        for ang_mom in range(0, max_angmom + 1):
            ao_phase.append([1.0] * (2*ang_mom + 1))
        return ao_phase

    def set_order(self, ang_mom, order):
        """Set a new subshell ordering for momentum quantum number ang_mom.

        Parameters
        ----------
        ang_mom: int
            The angular momentum quantun number
        order : tuple
            List of ml numbers

        """
        if self.max_angmom < ang_mom:
            raise ValueError(f'Angular momenta value ang_mom= {ang_mom}'
                             f' exceeds max_angmom = {self.max_angmom}')
        elif len(order) != 2*ang_mom + 1:
            raise TypeError(f'Wrong number of elements {len(order)} in '
                            f'order. Should be 2*ang_mom+1 = {2*ang_mom+1}')
        elif sum(order) != 0:
            raise ValueError(f'Wrongly defined order {order}. Should '
                             f'contain all values from -ang_mom to +ang_mom')
        else:
            self.ao_order[ang_mom] = order

    def set_phase(self, ang_mom, phase):
        """Set a new subshell phase for momentum quantum number ang_mom.

        Parameters
        ----------
        ang_mom: int
            The angular momentum quantun number
        phase : tuple
            List of ml numbers

        """
        if self.max_angmom < ang_mom:
            raise ValueError(f'Angular momenta value ang_mom= {ang_mom} '
                             f'exceeds max_angmom = {self.max_angmom}')
        elif len(phase) != 2*ang_mom + 1:
            raise TypeError(f'Wrong number of elements {len(phase)} in '
                            f'phase. Should be 2*ang_mom+1 = {2*ang_mom+1}')
        elif sum(abs(x) for x in phase) != float(2*ang_mom + 1):
            raise ValueError(f'Wrongly defined phase {phase}. All values'
                             ' should be 1.0 or -1.0')
        else:
            self.ao_phase[ang_mom] = phase

    def set_xyz_order(self):
        """Set the p-orbital ordering to be {px, py, pz}."""
        if self.max_angmom >= 1:
            self.set_order(1, [1, -1, 0])

    def __num_ao(self):
        """Return number of AOs."""
        num = 0
        for ang_mom in range(0, self.max_angmom + 1):
            num = num + self.ao_list[ang_mom] * (2*ang_mom + 1)
        return num

    def __make_tuple_ao(self, atom=0):
        """Return list of the unique tuples of each AO index.

        Parameters
        ----------
        atom : int
            Atomic index. Needed for a global mapping only

        """
        tuple_ao = [0] * self.num_ao
        ao = 0
        # Postpends augmented functions
        n_aug = self.augmentation
        if self.ordering == L_N_ML:
            for ang_mom in range(0, self.max_angmom + 1):
                for n in range(0, self.ao_list[ang_mom] - n_aug):
                    for ml in self.ao_order[ang_mom]:
                        tuple_ao[ao] = (atom, ang_mom, n, ml)
                        ao = ao + 1
            # Postpend augmented functions
            for ang_mom in range(0, self.max_angmom + 1):
                n_ang = self.ao_list[ang_mom]
                for n in range(n_ang - n_aug, n_ang):
                    for ml in self.ao_order[ang_mom]:
                        tuple_ao[ao] = (atom, ang_mom, n, ml)
                        ao = ao + 1
        elif self.ordering == L_ML_N:
            for ang_mom in range(0, self.max_angmom + 1):
                for ml in self.ao_order[ang_mom]:
                    for n in range(0, self.ao_list[ang_mom] - n_aug):
                        tuple_ao[ao] = (atom, ang_mom, n, ml)
                        ao = ao + 1
            # Postpend augmented functions
            for ang_mom in range(0, self.max_angmom + 1):
                for ml in self.ao_order[ang_mom]:
                    n_ang = self.ao_list[ang_mom]
                    for n in range(n_ang - n_aug, n_ang):
                        tuple_ao[ao] = (atom, ang_mom, n, ml)
                        ao = ao + 1
        return tuple_ao

    def __make_phase_ao(self):
        """Return list of the phase of each AO."""
        phase_ao = [0] * self.num_ao
        ao = 0
        # Postpends augmented functions
        n_aug = self.augmentation
        if self.ordering == L_N_ML:
            for ang_mom in range(0, self.max_angmom + 1):
                for n in range(0, self.ao_list[ang_mom] - n_aug):
                    for phase in self.ao_phase[ang_mom]:
                        phase_ao[ao] = phase
                        ao = ao + 1
            # Postpend augmented functions
            for ang_mom in range(0, self.max_angmom + 1):
                n_ang = self.ao_list[ang_mom]
                for n in range(n_ang - n_aug, n_ang):
                    for phase in self.ao_phase[ang_mom]:
                        phase_ao[ao] = phase
                        ao = ao + 1
        else:
            for ang_mom in range(0, self.max_angmom + 1):
                for phase in self.ao_phase[ang_mom]:
                    for n in range(0, self.ao_list[ang_mom] - n_aug):
                        phase_ao[ao] = phase
                        ao = ao + 1
            # Postpend augmented functions
            for ang_mom in range(0, self.max_angmom + 1):
                for phase in self.ao_phase[ang_mom]:
                    n_ang = self.ao_list[ang_mom]
                    for n in range(n_ang - n_aug, n_ang):
                        phase_ao[ao] = phase
                        ao = ao + 1
        return phase_ao

    @staticmethod
    def __atom_ao_mapping(atom1, atom2):
        """Make index and phase mapping between two atom types.

        Parameters
        ----------
        atom1 : AOAtomType
            The first AtomType
        atom2 : AOAtomType
            The second AtomType

        """
        if atom1.ao_list != atom2.ao_list:
            raise ValueError(f'Mismatch between the two AOAtomType to be'
                             f' mapped: {atom1.ao_list} != {atom2.ao_list}')
        else:
            mapping = [0] * atom1.num_ao
            phase = [1.0] * atom1.num_ao
            tuple1 = atom1.__make_tuple_ao()
            tuple2 = atom2.__make_tuple_ao()
            phase1 = atom1.__make_phase_ao()
            phase2 = atom2.__make_phase_ao()
            for i in range(0, atom1.num_ao):
                j = tuple2.index(tuple1[i])
                mapping[i] = j
                phase[i] = phase1[i] * phase2[j]
        return mapping, phase

    @staticmethod
    def allowed_orders():
        """Return the list of predefined AO orders.

        Returns
        -------
        List of predefined AO orders

        """
        return ALLOWED_ORDERS

    @staticmethod
    def atom_ao_transformation_matrix(atom1, atom2):
        """Make a transformation matrix to map AOs between two atom types.

        Resulting transformation matrix T transform from AO2 basis to AO1
        basis by acting the right:

             v1 = T * v2

        Parameters
        ----------
        atom1 : AOAtomType
            The first AtomType
        atom2 : AOAtomType
            The second AtomType

        """
        mapping, phase = AOAtomType.__atom_ao_mapping(atom1, atom2)
        n = atom1.num_ao
        t_mat = np.zeros((n, n))
        for i in range(0, n):
            t_mat[i][mapping[i]] = phase[i]
        return t_mat
